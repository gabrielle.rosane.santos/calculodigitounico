# Cálculo Dígito Único

# Recursos Usados

*  Database H2
*  Swagger - http://localhost:8080/swagger-ui.html#/
*  JPA/HIBERNATE
*  Maven

# Build Instructions

*  O start da aplicação deve ser feito com o Maven command: mvn install;
*  Suíte de testes mvn test;

# Obs.:

* Foi criado um endpoint para descriptografar o dado (é necessário enviar o dado criptografado em byte[]);
![](images/decriptar.jpeg)



