package br.com.inter.desafio.java.calcula.digito.unico.model;

import java.util.ArrayList;
import java.util.List;

/*
 * @author GabrielleRSAlbergaria
 */

public class OutputDigitoUnico {

	List<String> mensagens = new ArrayList<>();
	Integer resultado;
	
	public List<String> getMensagens() {
		return mensagens;
	}
	public void setMensagens(List<String> mensagens) {
		this.mensagens = mensagens;
	}
	public Integer getResultado() {
		return resultado;
	}
	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	} 

}
