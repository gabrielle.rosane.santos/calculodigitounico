package br.com.inter.desafio.java.calcula.digito.unico.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

}
