package br.com.inter.desafio.java.calcula.digito.unico.model;

import com.sun.istack.NotNull;

/*
 * @author GabrielleRSAlbergaria
 */

public class InputDigitoUnico {

	@NotNull
	private Integer parametroN;
	@NotNull
	private Integer parametroK;

	private String idUser;

	public Integer getParametroN() {
		return parametroN;
	}

	public void setParametroN(Integer parametroN) {
		this.parametroN = parametroN;
	}

	public Integer getParametroK() {
		return parametroK;
	}

	public void setParametroK(Integer parametroK) {
		this.parametroK = parametroK;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

}
