package br.com.inter.desafio.java.calcula.digito.unico.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sun.istack.NotNull;

/*
 * @author GabrielleRSAlbergaria
 */

@Entity
public class Usuario {

	@JsonInclude(Include.NON_NULL)
	@NotNull
	private String nome;
	@JsonInclude(Include.NON_NULL)
	@Id
	@NotNull
	private String email;
	@JsonInclude(Include.NON_NULL)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
	private List<DigitoUnico> digitosUnicos;

	public String getNome() {
		return nome;
	}

	public void setNome(String name) {
		this.nome = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DigitoUnico> getDigitosUnicos() {
		return digitosUnicos;
	}

	public void setDigitosUnicos(List<DigitoUnico> digitosUnicos) {
		this.digitosUnicos = digitosUnicos;
	}

}
