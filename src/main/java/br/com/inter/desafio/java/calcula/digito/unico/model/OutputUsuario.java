package br.com.inter.desafio.java.calcula.digito.unico.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OutputUsuario {

	@JsonInclude(Include.NON_NULL)
	List<OutputBodyUsuario> usuarios = new ArrayList<OutputBodyUsuario>();
	String resultado = "";

	public OutputUsuario() {

	}

	public OutputUsuario(List<OutputBodyUsuario> usuarios, String resultado) {
		super();
		this.usuarios = usuarios;
		this.resultado = resultado;
	}

	public List<OutputBodyUsuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<OutputBodyUsuario> usuarios) {
		this.usuarios = usuarios;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
