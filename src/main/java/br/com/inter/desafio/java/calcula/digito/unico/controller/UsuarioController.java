package br.com.inter.desafio.java.calcula.digito.unico.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.desafio.java.calcula.digito.unico.model.InputBodyDescriptografia;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputUsuario;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;
import br.com.inter.desafio.java.calcula.digito.unico.repository.UsuarioRepository;
import br.com.inter.desafio.java.calcula.digito.unico.service.UsuarioService;
import br.com.inter.desafio.java.calcula.digito.unico.util.EncriptaDecripta;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

// Create, Read, Update and Delete
@RestController
@RequestMapping(value = "/api")
@Api(value = "Usuário Controller")
@CrossOrigin(origins = "*")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/users")
	@ApiOperation(value = "Retorna todos os usuários cadastrados")
	public OutputUsuario GetAllUsers() {
		return usuarioService.recuperarTodosUsuariosCadastrados();
	}

	@ApiOperation(value = "Retorna um usuário por id.")
	@GetMapping("/user/{id}")
	public ResponseEntity<OutputUsuario> recuperarUsuarioPorId(@PathVariable(value = "id") String id) {
		OutputUsuario userOutput = new OutputUsuario();
		userOutput = usuarioService.buscarUsuarioPorId(id);
		if (userOutput.getUsuarios().size() != 0)
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.OK);
		else
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.NOT_FOUND);
	}

	@ApiOperation(value = "Cria usuário")
	@PostMapping("/user")
	public ResponseEntity<OutputUsuario> criarUsuario(@RequestBody Usuario novoUsuario) {
		OutputUsuario userOutput = new OutputUsuario();
		userOutput = usuarioService.createUser(novoUsuario);
		if (userOutput.getUsuarios().size() != 0)
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.OK);
		else
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.UNAUTHORIZED);
	}

	@ApiOperation(value = "Deleta usuário")
	@DeleteMapping("/user/{id}")
	public ResponseEntity<Object> deletarUsuario(@PathVariable(value = "id") String id) {
		Optional<Usuario> user = usuarioRepository.findById(id);
		if (user.isPresent()) {
			usuarioRepository.delete(user.get());
			EncriptaDecripta encriptaDecripta = new EncriptaDecripta();
			encriptaDecripta.deletarChaves(id);
			;
			return new ResponseEntity<>(HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@ApiOperation(value = "Atualiza o usuário.")
	@PutMapping("user/{id}")
	public ResponseEntity<OutputUsuario> atualizarUsuario(@PathVariable(value = "id") String id,
			@RequestBody Usuario updateUsuario) {
		OutputUsuario userOutput = new OutputUsuario();
		userOutput = usuarioService.atualizarUsuario(updateUsuario, id);
		if (userOutput.getUsuarios().size() != 0)
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.OK);
		else
			return new ResponseEntity<OutputUsuario>(userOutput, HttpStatus.NOT_FOUND);
	}

	@GetMapping("/user/key/{id}")
	@ApiOperation(value = "Retorna a chave RSA publica do usu'ario")
	public String recuperarKeyPublicRSA(@PathVariable(value = "id") String id) {
		return usuarioService.recuperarPublickeyRSA(id);
	}

	@ApiOperation(value = "Descriptografa dado")
	@PostMapping("/user/decriptar/dado")
	public String criarUsuario(@RequestBody InputBodyDescriptografia inputBodyDescriptografia) {
		return usuarioService.descriptografaDado(inputBodyDescriptografia);
	}

}
