package br.com.inter.desafio.java.calcula.digito.unico.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.desafio.java.calcula.digito.unico.model.DigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.InputDigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputDigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;
import br.com.inter.desafio.java.calcula.digito.unico.repository.DigitoUnicoRepository;
import br.com.inter.desafio.java.calcula.digito.unico.repository.UsuarioRepository;
import br.com.inter.desafio.java.calcula.digito.unico.service.DigitoUnicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/*
 * @author GabrielleRSAlbergaria
 */

@RestController
@RequestMapping(value = "/api")
@Api(value = "Digito Único Controller")
@CrossOrigin(origins = "*")
public class DigitoUnicoController {

	@Autowired
	private DigitoUnicoService digitoUnicoService;
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;
	@Autowired
	private UsuarioRepository userRepository;

	
	@PostMapping("/calcularDigitoUnico")
	@ApiOperation(value = "Calcula dígito único.")
	public OutputDigitoUnico calcularDigitoUnico(@RequestBody InputDigitoUnico inputDigitoUnico) {
		return digitoUnicoService.checarCache(inputDigitoUnico);
	}

	@GetMapping("/recuperaTodosOsCalculos/{id}")
	@ApiOperation(value = "Retorna todos os dígitos únicos calculados por um usuário.")
	public ResponseEntity<List<DigitoUnico>> recuperarTodosOsCalculosPorUsuario(@PathVariable(value = "id") String id) {
		Optional<Usuario> user = userRepository.findById(id);
		if (user.isPresent()) {
			Optional<List<DigitoUnico>> digitosUnico = digitoUnicoRepository.recuperarTodosOsDigitosUnicosPorUsuario(user);
			return new ResponseEntity<List<DigitoUnico>>(digitosUnico.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

}
