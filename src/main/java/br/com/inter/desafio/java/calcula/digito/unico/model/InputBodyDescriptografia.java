package br.com.inter.desafio.java.calcula.digito.unico.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * @author GabrielleRSAlbergaria
 */

public class InputBodyDescriptografia {

	private byte[] dado;
	@JsonInclude(Include.NON_NULL)
	String email;
	public byte[] getDado() {
		return dado;
	}
	public void setDado(byte[] dado) {
		this.dado = dado;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
