package br.com.inter.desafio.java.calcula.digito.unico.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/*
 * @author GabrielleRSAlbergaria
 */

public class OutputBodyUsuario {

	@JsonInclude(Include.NON_NULL)
	private byte[] nome;
	@JsonInclude(Include.NON_NULL)
	byte[] email;
	@JsonInclude(Include.NON_NULL)
	private List<DigitoUnico> digitosUnicos;

	public byte[] getNome() {
		return nome;
	}

	public void setNome(byte[] nome) {
		this.nome = nome;
	}

	public byte[] getEmail() {
		return email;
	}

	public void setEmail(byte[] email) {
		this.email = email;
	}

	public List<DigitoUnico> getDigitosUnicos() {
		return digitosUnicos;
	}

	public void setDigitosUnicos(List<DigitoUnico> digitosUnicos) {
		this.digitosUnicos = digitosUnicos;
	}

}
