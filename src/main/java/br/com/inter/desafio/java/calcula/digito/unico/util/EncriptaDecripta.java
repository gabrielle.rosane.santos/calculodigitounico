package br.com.inter.desafio.java.calcula.digito.unico.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class EncriptaDecripta {

	public static final String ALGORITHM = "RSA";

	public static String PATH_CHAVE_PRIVADA = "";
	public static String PATH_CHAVE_PUBLICA = "";

	public static void setNomesArquivos(String email) {
		PATH_CHAVE_PRIVADA = "./keys/emailprivate.key";
		PATH_CHAVE_PUBLICA = "./keys/emailpublic.key";
		PATH_CHAVE_PRIVADA = PATH_CHAVE_PRIVADA.replace("email", email);
		PATH_CHAVE_PUBLICA = PATH_CHAVE_PUBLICA.replace("email", email);
	}

	public void geraChave(String email) {
		try {
			final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
			keyGen.initialize(2048);
			final KeyPair key = keyGen.generateKeyPair();

			File chavePrivadaFile = new File(PATH_CHAVE_PRIVADA);
			File chavePublicaFile = new File(PATH_CHAVE_PUBLICA);

			if (chavePrivadaFile.getParentFile() != null) {
				chavePrivadaFile.getParentFile().mkdirs();
			}

			chavePrivadaFile.createNewFile();

			if (chavePublicaFile.getParentFile() != null) {
				chavePublicaFile.getParentFile().mkdirs();
			}

			chavePublicaFile.createNewFile();

			ObjectOutputStream chavePublicaOS = new ObjectOutputStream(new FileOutputStream(chavePublicaFile));
			chavePublicaOS.writeObject(key.getPublic());
			chavePublicaOS.close();

			ObjectOutputStream chavePrivadaOS = new ObjectOutputStream(new FileOutputStream(chavePrivadaFile));
			chavePrivadaOS.writeObject(key.getPrivate());
			chavePrivadaOS.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean verificaSeExisteChavesNoSO(String email) {

		setNomesArquivos(email);

		File chavePrivada = new File(PATH_CHAVE_PRIVADA);
		File chavePublica = new File(PATH_CHAVE_PUBLICA);

		if (chavePrivada.exists() && chavePublica.exists()) {
			return true;
		}

		return false;
	}

	public static byte[] criptografa(String texto, PublicKey chave) {
		byte[] cipherText = null;

		try {
			final Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, chave);
			cipherText = cipher.doFinal(texto.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cipherText;
	}

	public String decriptografa(byte[] texto, PrivateKey chave) {
		byte[] dectyptedText = null;

		try {
			final Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, chave);
			dectyptedText = cipher.doFinal(texto);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(dectyptedText);
	}

	public byte[] retornarTextoCriptografado(String texto) {
		byte[] textoCriptografado = null;
		try {
			ObjectInputStream inputStream = null;
			inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PUBLICA));
			final PublicKey chavePublica = (PublicKey) inputStream.readObject();
			textoCriptografado = criptografa(texto, chavePublica);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return textoCriptografado;

	}

	public String retornarTextoDescriptografado(byte[] textoCriptografado) {
		ObjectInputStream inputStream = null;
		String textoPuro = "";
		try {
			inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PRIVADA));
			final PrivateKey chavePrivada = (PrivateKey) inputStream.readObject();
			textoPuro = decriptografa(textoCriptografado, chavePrivada);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return textoPuro;
	}

	public void deletarChaves(String email) {

		setNomesArquivos(email);
		File chavePrivada = new File(PATH_CHAVE_PRIVADA);
		File chavePublica = new File(PATH_CHAVE_PUBLICA);

		if (chavePrivada.exists() && chavePublica.exists()) {
			chavePrivada.delete();
			chavePublica.delete();
		}

	}

	public String retornarPublicKeyRSA(String id) {
		setNomesArquivos(id);

		File chavePrivada = new File(PATH_CHAVE_PRIVADA);
		File chavePublica = new File(PATH_CHAVE_PUBLICA);

		if (chavePrivada.exists() && chavePublica.exists()) {
			try {
				ObjectInputStream inputStream = null;
				inputStream = new ObjectInputStream(new FileInputStream(PATH_CHAVE_PUBLICA));
				final PublicKey chavePublicaRSA = (PublicKey) inputStream.readObject();
				inputStream.close();
				return chavePublicaRSA.toString();

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return "Chave not found";
	}
}
