package br.com.inter.desafio.java.calcula.digito.unico.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.inter.desafio.java.calcula.digito.unico.model.InputDigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputDigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.DigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;
import br.com.inter.desafio.java.calcula.digito.unico.repository.DigitoUnicoRepository;
import br.com.inter.desafio.java.calcula.digito.unico.repository.UsuarioRepository;
import br.com.inter.desafio.java.calcula.digito.unico.util.CacheDigitoUnico;

@Service("DigitoUnicoService")
public class DigitoUnicoService {

	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@Autowired
	private UsuarioRepository userRepository;

	private CacheDigitoUnico cache = new CacheDigitoUnico();

	public OutputDigitoUnico checarCache(InputDigitoUnico InputDigitoUnico) {
		OutputDigitoUnico outputDigitoUnico = new OutputDigitoUnico();
		Integer resultadoDigitoUnico;

		Integer cacheResultado = cache.buscarElementoNaCache(InputDigitoUnico);

		if (!(cacheResultado.equals(-1))) {
			resultadoDigitoUnico = cacheResultado;
		} else {
			resultadoDigitoUnico = this.calcularDigitoUnico(InputDigitoUnico.getParametroN().toString(),
					InputDigitoUnico.getParametroK());
			cache.adicionarElemento(new DigitoUnico(InputDigitoUnico.getParametroN(), InputDigitoUnico.getParametroK(),
					resultadoDigitoUnico));
		}

		Optional<Usuario> usuario = userRepository.findById(InputDigitoUnico.getIdUser());

		if (usuario.isPresent()) {

			Optional<DigitoUnico> pesquisaDigitoUnico = digitoUnicoRepository.recuperarDigitoUnicoPorParametros( //
					InputDigitoUnico.getParametroN(), InputDigitoUnico.getParametroK(), usuario);

			if (pesquisaDigitoUnico.isPresent() == false) {
				DigitoUnico digitoUnico = new DigitoUnico(InputDigitoUnico.getParametroN(), //
						InputDigitoUnico.getParametroK()); //
				digitoUnico.setResultado(resultadoDigitoUnico);
				Usuario usuarioC = new Usuario();
				usuarioC.setEmail(usuario.get().getEmail());
				digitoUnico.setUsuario(usuarioC);
				digitoUnicoRepository.save(digitoUnico);
			}
		}

		outputDigitoUnico.setResultado(resultadoDigitoUnico);
		return outputDigitoUnico;

	}

	public int calcularDigitoUnico(String parameterN, Integer parameterK) {

		String singleDigit = "";

		if (parameterN.toString().length() == 1 && parameterK <= 1) {
			return Integer.parseInt(parameterN);
		} else if (parameterK > 1) {
			for (int i = 0; i < parameterK; i++) {
				singleDigit += parameterN.toString();
			}
			parameterN = singleDigit;
			parameterK = 0;
		} else {
			Integer digit = 0;
			for (int i = 0; i < parameterN.length(); i++) {
				digit = digit + Integer.parseInt((parameterN.charAt(i) + ""));
			}
			parameterN = digit.toString();
		}
		return calcularDigitoUnico(parameterN, parameterK);
	}

}