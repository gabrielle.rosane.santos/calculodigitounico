package br.com.inter.desafio.java.calcula.digito.unico.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.inter.desafio.java.calcula.digito.unico.model.DigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Integer> {

	@Query("SELECT d FROM DigitoUnico d WHERE d.parametroN = ?1 and d.parametroK = ?2 and d.usuario = ?3")
	Optional<DigitoUnico> recuperarDigitoUnicoPorParametros(Integer parametroN, Integer parametroK, Optional<Usuario> usuario);


	@Query("SELECT d FROM DigitoUnico d WHERE d.usuario = ?1")
	Optional<List<DigitoUnico>> recuperarTodosOsDigitosUnicosPorUsuario( Optional<Usuario> usuario);
}

