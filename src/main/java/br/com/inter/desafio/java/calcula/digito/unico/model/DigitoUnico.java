package br.com.inter.desafio.java.calcula.digito.unico.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sun.istack.NotNull;

/*
 * @author GabrielleRSAlbergaria
 */

@Entity
public class DigitoUnico {

	@JsonInclude(Include.NON_NULL)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	@JsonInclude(Include.NON_NULL)
	@NotNull
	private Integer parametroN;
	@JsonInclude(Include.NON_NULL)
	@NotNull
	private Integer parametroK;
	@JsonInclude(Include.NON_NULL)
	@NotNull
	private Integer resultado;
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = true)
	@JsonIgnore
	private Usuario usuario;

	public DigitoUnico() {

	}

	public DigitoUnico(Integer parameterN, Integer parameterK) {
		super();
		this.parametroN = parameterN;
		this.parametroK = parameterK;
	}

	public DigitoUnico(Integer parameterN, Integer parameterK, Integer result) {
		super();
		this.parametroN = parameterN;
		this.parametroK = parameterK;
		this.resultado = result;
	}

	public Integer getParametroN() {
		return parametroN;
	}

	public void setParametroN(Integer parametroN) {
		this.parametroN = parametroN;
	}

	public Integer getParametroK() {
		return parametroK;
	}

	public void setParametroK(Integer parametroK) {
		this.parametroK = parametroK;
	}

	public Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getId() {
		return id;
	}

}
