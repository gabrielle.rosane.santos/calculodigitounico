package br.com.inter.desafio.java.calcula.digito.unico.util;

import java.util.ArrayList;

import br.com.inter.desafio.java.calcula.digito.unico.model.DigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.InputDigitoUnico;

public class CacheDigitoUnico {

	ArrayList<DigitoUnico> cache = new ArrayList<DigitoUnico>(10);

	private void removerPrimeiroElemento() {
		cache.remove(0);
	}

	public void adicionarElemento(DigitoUnico digitounico) {
		if (cache.size() >= 10) {
			removerPrimeiroElemento();
		}
		cache.add(digitounico);
	}

	public Integer buscarElementoNaCache(InputDigitoUnico digitoUnico) {

		for (DigitoUnico digitoUnicoSearch : cache) {
			if (digitoUnicoSearch.getParametroK().equals(digitoUnico.getParametroK())
					&& digitoUnicoSearch.getParametroN().equals(digitoUnico.getParametroN())) {
				return digitoUnicoSearch.getResultado();
			}
		}

		return -1;
	}

	public ArrayList<DigitoUnico> getCache() {
		return cache;
	}

	public void setCache(ArrayList<DigitoUnico> cache) {
		this.cache = cache;
	}

}
