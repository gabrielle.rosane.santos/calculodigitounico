package br.com.inter.desafio.java.calcula.digito.unico.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.inter.desafio.java.calcula.digito.unico.model.InputBodyDescriptografia;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputBodyUsuario;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputUsuario;
import br.com.inter.desafio.java.calcula.digito.unico.model.Usuario;
import br.com.inter.desafio.java.calcula.digito.unico.repository.UsuarioRepository;
import br.com.inter.desafio.java.calcula.digito.unico.util.EncriptaDecripta;

@Service("UserService")
public class UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;

	private final String USER_ALREADY_EXISTS = "Já existe um cadastro com o email informado.";
	private final String USER_CREATED_SUCCESS = "Usuário criado com sucesso!";
	private final String USER_SEARCHED_SUCCESS = "Usuário recuperado com sucesso!";
	private final String USER_NOT_FOUND = "Usuário não cadastrado!";
	private final String USER_NOT_SEARCHED = "Usuários não recuperados.";
	private final String USERS_SEARCHED_SUCCESS = "Usuários recuperados com sucesso!";
	private final String USER_UPDATE_SUCCESS = "Usuários recuperados com sucesso!";

	public OutputUsuario createUser(Usuario newUserInput) {

		OutputUsuario outputUsuario = new OutputUsuario();
		OutputBodyUsuario newUser = new OutputBodyUsuario();

		Optional<Usuario> user = usuarioRepository.findById(newUserInput.getEmail());

		if (user.isPresent()) {
			outputUsuario.setResultado(USER_ALREADY_EXISTS);
		} else if (newUserInput.getEmail() != null) {
			usuarioRepository.save(newUserInput);
			newUser = encryptaSaida(newUserInput.getEmail(), newUserInput.getNome());
			outputUsuario.getUsuarios().add(newUser);
			outputUsuario.setResultado(USER_CREATED_SUCCESS);
		}

		return outputUsuario;

	}

	public OutputUsuario buscarUsuarioPorId(String id) {
		OutputUsuario outputUsuario = new OutputUsuario();
		OutputBodyUsuario usuario = new OutputBodyUsuario();

		Optional<Usuario> usuarioSearch = usuarioRepository.findById(id);
		if (usuarioSearch.isPresent()) {
			usuario = encryptaSaida(usuarioSearch.get().getEmail(), usuarioSearch.get().getNome());
			outputUsuario.setResultado(USER_SEARCHED_SUCCESS);
			usuario.setDigitosUnicos(usuarioSearch.get().getDigitosUnicos());
			outputUsuario.getUsuarios().add(usuario);
		} else {
			outputUsuario.setResultado(USER_NOT_FOUND);
		}
		return outputUsuario;

	}

	public OutputUsuario recuperarTodosUsuariosCadastrados() {
		OutputUsuario outputUsuario = new OutputUsuario();
		List<OutputBodyUsuario> usuarios = new ArrayList<OutputBodyUsuario>();
		List<Usuario> usuariosRepository = usuarioRepository.findAll();
		for (Usuario usuario : usuariosRepository) {
			usuarios.add(encryptaSaida(usuario.getEmail(), usuario.getNome()));
		}
		if (usuarios.size() != 0) {
			outputUsuario.setResultado(USERS_SEARCHED_SUCCESS);
		} else {
			outputUsuario.setResultado(USER_NOT_SEARCHED);
		}
		outputUsuario.setUsuarios(usuarios);
		return outputUsuario;
	}

	public OutputBodyUsuario encryptaSaida(String email, String nome) {

		EncriptaDecripta encriptaDecriptaRSA = new EncriptaDecripta();
		OutputBodyUsuario userEncryptado = new OutputBodyUsuario();
		if (!encriptaDecriptaRSA.verificaSeExisteChavesNoSO(email)) {
			encriptaDecriptaRSA.geraChave(email);
		}
		userEncryptado.setEmail(encriptaDecriptaRSA.retornarTextoCriptografado(email));
		userEncryptado.setNome(encriptaDecriptaRSA.retornarTextoCriptografado(nome));

		return userEncryptado;
	}

	public OutputUsuario atualizarUsuario(Usuario usuario, String id) {
		OutputUsuario outputUsuario = new OutputUsuario();
		Optional<Usuario> usuarioSearch = usuarioRepository.findById(id);
		OutputBodyUsuario updateUsuario = new OutputBodyUsuario();

		if (usuarioSearch.isPresent()) {
			Usuario oldUsuario = usuarioSearch.get();
			oldUsuario.setNome(usuario.getNome());
			usuarioRepository.save(oldUsuario);
			updateUsuario = encryptaSaida(oldUsuario.getEmail(), oldUsuario.getNome());
			outputUsuario.setResultado(USER_UPDATE_SUCCESS);
			outputUsuario.getUsuarios().add(updateUsuario);
		} else {
			outputUsuario.setResultado(USER_NOT_FOUND);
		}
		return outputUsuario;
	}

	public String recuperarPublickeyRSA(String id) {
		EncriptaDecripta encriptaDecripta = new EncriptaDecripta();
		return encriptaDecripta.retornarPublicKeyRSA(id);
	}

	public String descriptografaDado(InputBodyDescriptografia inputBodyDescriptografia) {
		EncriptaDecripta encriptaDecripta = new EncriptaDecripta();
		if (encriptaDecripta.verificaSeExisteChavesNoSO(inputBodyDescriptografia.getEmail())) {
			return encriptaDecripta.retornarTextoDescriptografado(inputBodyDescriptografia.getDado());
		}
		return "Não foi encontrada chave salva para seu email";
	}
}