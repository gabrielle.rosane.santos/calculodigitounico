package br.com.inter.desafio.java.calcula.digito.unico.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import br.com.inter.desafio.java.calcula.digito.unico.model.OutputBodyUsuario;
import br.com.inter.desafio.java.calcula.digito.unico.model.OutputUsuario;
import br.com.inter.desafio.java.calcula.digito.unico.repository.DigitoUnicoRepository;
import br.com.inter.desafio.java.calcula.digito.unico.repository.UsuarioRepository;
import br.com.inter.desafio.java.calcula.digito.unico.service.DigitoUnicoService;
import br.com.inter.desafio.java.calcula.digito.unico.service.UsuarioService;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

@WebMvcTest
class UsuarioControllerTest {

	@Autowired
	private UsuarioController userController = new UsuarioController();

	@MockBean
	private UsuarioRepository usuarioRepository;

	@MockBean
	private DigitoUnicoRepository digitoUnicoRepository;

	@MockBean
	private DigitoUnicoService DigitoUnicoService;

	@MockBean
	private UsuarioService usuarioService;

	@BeforeEach
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(this.userController);
	}

	@Test
	public void DeveRetornarSucesso_QuandoCriarUsuario() {
		List<OutputBodyUsuario> usuarios = new ArrayList<OutputBodyUsuario>();
		OutputUsuario outputUsuario = new OutputUsuario(usuarios, "");
		OutputBodyUsuario usuario1 = new OutputBodyUsuario();
		usuario1.setNome("USUARIO1".getBytes());
		usuario1.setEmail("USUARIO1".getBytes());
		usuarios.add(usuario1);
		OutputBodyUsuario usuario2 = new OutputBodyUsuario();
		usuario2.setNome("USUARIO1".getBytes());
		usuario2.setEmail("USUARIO1".getBytes());
		usuarios.add(usuario2);
		outputUsuario.setUsuarios(usuarios);

		when(this.usuarioService.recuperarTodosUsuariosCadastrados()).thenReturn(outputUsuario);

		given().accept(ContentType.JSON).when().get("/api/users").then().statusCode(HttpStatus.OK.value());
		
	}
	
	@Test
	public void DeveRetornarSucesso_QuandoBuscarUmUsuário() {
		
		OutputUsuario outputUsuario = new OutputUsuario();
		OutputBodyUsuario usuario1 = new OutputBodyUsuario();
		usuario1.setNome("USUARIO1".getBytes());
		usuario1.setEmail("USUARIO1".getBytes());
		outputUsuario.getUsuarios().add(usuario1);
		
		when(this.usuarioService.buscarUsuarioPorId("1")).thenReturn(outputUsuario);

		given().accept(ContentType.JSON).when().get("/api/users").then().statusCode(HttpStatus.OK.value());
		
	}

}
