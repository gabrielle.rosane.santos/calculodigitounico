package br.com.inter.desafio.java.calcula.digito.unico.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EncriptaDecriptaTest {

	private EncriptaDecripta encriptaDecripta = new EncriptaDecripta();

	@Test
	public void DeveRetornarTextoOriginal_QuandoEncriptarEDescriptar() {

		if (!encriptaDecripta.verificaSeExisteChavesNoSO("testeChave")) {
			encriptaDecripta.geraChave("testeChave");
		}

		final String texto = "Mensagem para ser criptografada e enviada";
		byte[] textoCriptografado = encriptaDecripta.retornarTextoCriptografado(texto);

		assertEquals(texto, encriptaDecripta.retornarTextoDescriptografado(textoCriptografado));

	}
}
