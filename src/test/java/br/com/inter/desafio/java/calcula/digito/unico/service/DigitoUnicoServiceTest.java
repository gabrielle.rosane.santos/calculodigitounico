package br.com.inter.desafio.java.calcula.digito.unico.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DigitoUnicoServiceTest {

	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@Test
	public void mustReturnResultado_whenCalculateSingleDigit() {
		Integer parameterN = 9785;
		assertEquals(8, digitoUnicoService.calcularDigitoUnico(parameterN.toString(), 4));
	}

	@Test
	public void mustReturnTheSameParameterN_whenCalculateSingleDigit() {
		Integer parameterN = 9;
		assertEquals(9, digitoUnicoService.calcularDigitoUnico(parameterN.toString(), 4));
	}

}
