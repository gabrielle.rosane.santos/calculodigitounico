package br.com.inter.desafio.java.calcula.digito.unico.util;



import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.inter.desafio.java.calcula.digito.unico.model.DigitoUnico;
import br.com.inter.desafio.java.calcula.digito.unico.model.InputDigitoUnico;

@SpringBootTest
class CacheDigitoUnicoTest {

	private CacheDigitoUnico cacheDigitoUnico = new CacheDigitoUnico();
	
	@Test
	public void DeveRetornarTamanhoListaCacheIgual10_QuandoAdicionarMaximo11() {
		DigitoUnico digitoUnico = new DigitoUnico();
		digitoUnico.setParametroK(10);
		digitoUnico.setParametroN(20);
		digitoUnico.setResultado(120);

		for(int i =0; i< 12; i++) {
		cacheDigitoUnico.adicionarElemento(digitoUnico);
		}
		assertEquals(10,cacheDigitoUnico.getCache().size());

	}
	@Test
	public void DeveRetornarNumeroNegativo_QuandoProcurarUmCalculoNaoArmazenadoNaCache() {
		DigitoUnico digitoUnico = new DigitoUnico();
		digitoUnico.setParametroK(10);
		digitoUnico.setParametroN(20);
		digitoUnico.setResultado(120);

		for(int i =0; i< 12; i++) {
		cacheDigitoUnico.adicionarElemento(digitoUnico);
		}
		InputDigitoUnico digitoUnicoResultado = new InputDigitoUnico();
		digitoUnico.setParametroK(2);
		digitoUnico.setParametroK(3);

		assertEquals(-1,cacheDigitoUnico.buscarElementoNaCache(digitoUnicoResultado));

	}

}
